TARGET_EXEC := regular

BUILD_DIR := ./build
SRC_DIR := ./src

INCLUDE_DIRS := /usr/lib64
LIB_DIRS :=
LIBS := stdc++ m pthread boost_system boost_atomic boost_filesystem boost_regex

SRCS := $(shell find $(SRC_DIR) -name '*.cpp' -or -name '*.c' -or -name '*.cxx')

OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)

DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIR) -type d)
INC_DIRS += $(INCLUDE_DIRS)

INC_FLAGS := $(addprefix -I,$(INC_DIRS))

LDFLAGS += $(addprefix -L,$(LIB_DIRS))
LDFLAGS += $(addprefix -l,$(LIBS))

CPPFLAGS := $(INC_FLAGS) -MMD -MP -std=c++11

#CC := arm-none-linux-gnueabihf-g++
#CXX := arm-none-linux-gnueabihf-g++
CXX := g++

# The final build step.
$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CC) $(OBJS) -o $@ $(LDFLAGS)

# Build step for C source
$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

# Build step for C++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

# Build step for C++ source
$(BUILD_DIR)/%.cxx.o: %.cxx
	mkdir -p $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -r $(BUILD_DIR)

# Include the .d makefiles. The - at the front suppresses the errors of missing
# Makefiles. Initially, all the .d files will be missing, and we don't want those
# errors to show up.
-include $(DEPS)
