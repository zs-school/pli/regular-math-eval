#include <iostream>
#include <string>
#include <vector>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <fstream>
#include <exprtk.hpp>

using namespace std;

int main(int argc, char ** argv)
{
    auto path = boost::filesystem::current_path();
//    cout << path << endl;

    vector<boost::filesystem::path> valid_paths;

    for (const auto& file : boost::filesystem::directory_iterator(path))
    {
        string filepath = file.path().string();
//        cout << "File " << filepath << endl;

        boost::regex e(".*txt");
        if (boost::regex_match(filepath, e))
        {
            valid_paths.push_back(filepath);
        }
    }

    for (int i = 0; i < valid_paths.size(); i++)
    {
//        cout << "Valid file " << valid_paths[i] << endl;
        ifstream in;
        in.open(valid_paths[i].c_str());
        if (in.is_open())
        {
            string line;
            while(getline(in, line))
            {
                std::string::const_iterator start, end;
                start = line.begin();
                end = line.end();
//                cout << line << endl;
                boost::match_flag_type flags = boost::match_default;
                boost::regex e("\\b\\S*\\s\?=\\s\?\\d+");
                boost::smatch value;

                while (boost::regex_search(start, end, value, e, flags))
                {
//                    cout << value[0] << endl;
                    start = value[0].second;

                    exprtk::parser<double> parser;
                    exprtk::expression<double> expression;
                    parser.compile(value[0], expression);

                    double val = expression.value();
//                    cout << "Result " << val << endl;
                    cout << value[0] << " : " << (val ? "Correct" : "Wrong") << endl;
                }
            }
        }
    }


    return 0;
}
