Vytvořit program, který projde všechny soubory typu TXT z adresáře, v kterém je spuštěn (ne podadresáře), přičemž
a) každý soubor TXT otevře,
b) v otevřeném souboru nalene všechny řádky obsahující rovnici ve tvaru
textovy_retezec_bez_mezer = prirozene_cislo např. cos(0)+10-2*5 = 1
c) ověří, zda je rovnice matematicky správně. Vypíše hlášku o správnosti.
Např.: cos(0) = 1 : Správně
